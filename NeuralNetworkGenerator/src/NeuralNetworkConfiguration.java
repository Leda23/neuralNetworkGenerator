//package neuralNetworkModelInterpreter;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import org.isis.gme.bon.JBuilderModel;

import weka.classifiers.evaluation.Evaluation;

public class NeuralNetworkConfiguration implements Runnable{
	
	ModelInterpreter modelInterpreter;
	JBuilderModel tempNeuralNetwork;
	List<List<String>> hiddenNeuronsPerLayer;
	HashMap<String, String> topology;
	Evaluation evaluation;
	int indexToTrain;

	public NeuralNetworkConfiguration(ModelInterpreter modelInterpreter,int indexToTrain) {
		this.modelInterpreter=modelInterpreter;
		this.indexToTrain=indexToTrain;
		this.hiddenNeuronsPerLayer=this.modelInterpreter.getTempHiddenNeuronsPerLayer().get(indexToTrain);
		this.tempNeuralNetwork=this.modelInterpreter.getTempNeuralNetworkArray().get(indexToTrain);
		this.topology=this.modelInterpreter.getTempTopology().get(indexToTrain);
	}
	
	@Override
	public void run() {
		String dataFileName = this.modelInterpreter.getFilePathField();
		NeuralNetworkTrainer neuralNetworkTrainer=null;
		try{
			neuralNetworkTrainer = new NeuralNetworkTrainer(dataFileName);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		// Checking options
		neuralNetworkTrainer.setHiddenLayerNames(hiddenNeuronsPerLayer);
		String learningRate = this.modelInterpreter.getLearningRateField();
		if (learningRate.equals("")) {
			neuralNetworkTrainer.setLearningRate(0.3);
		} else {
			neuralNetworkTrainer.setLearningRate(Double.parseDouble(learningRate));
		}
		String momentum = this.modelInterpreter.getMomentumField();
		if (learningRate.equals("")) {
			neuralNetworkTrainer.setMomentum(0.2);
		} else {
			neuralNetworkTrainer.setMomentum(Double.parseDouble(momentum));
		}
		String numberOfEpoch = this.modelInterpreter.getEpochField();
		if (numberOfEpoch.equals("")) {
			neuralNetworkTrainer.setNumEpochs(500);
		} else {
			neuralNetworkTrainer.setNumEpochs(Integer.parseInt(numberOfEpoch));
		}
		String sizeValidation = this.modelInterpreter.getSizeValidationField();
		if (sizeValidation.equals("")) {
			neuralNetworkTrainer.setPercentValidation(0);
		} else {
			neuralNetworkTrainer.setPercentValidation(Integer.parseInt(sizeValidation));
		}
		String seed = this.modelInterpreter.getSeedField();
		if (seed.equals("")) {
			neuralNetworkTrainer.setSeed(0);
		} else {
			neuralNetworkTrainer.setSeed(Integer.parseInt(seed));
		}
		
		
		if ( this.modelInterpreter.getSubmit().equals("PMML")) {
			//test//
			try {
				neuralNetworkTrainer.buildNetwork();
				evaluation = new Evaluation(neuralNetworkTrainer.getTrainInstances());
				evaluation.crossValidateModel(neuralNetworkTrainer.getNetwork(), neuralNetworkTrainer.getTrainInstances(), 10,new Random(1));
				if(evaluation.rootMeanSquaredError()<this.modelInterpreter.getBestRootMeanSquaredError()){
					this.modelInterpreter.setBestIteration(indexToTrain);
					this.modelInterpreter.setBestRootMeanSquaredError(evaluation.rootMeanSquaredError());
					this.modelInterpreter.setBestMLP(neuralNetworkTrainer.getNetwork());
					this.modelInterpreter.setEvaluation(evaluation);
					this.modelInterpreter.setTrainingInstances(neuralNetworkTrainer.getTrainInstances());
					neuralNetworkTrainer.setNodeNamesToWeightName(topology);
					this.modelInterpreter.setNeuralNetworkTopology(neuralNetworkTrainer.getTopology());
				}
				/*else{
					int index=this.modelInterpreter.getRoot().getRootModels().indexOf(tempNeuralNetwork);
					if(index!=-1){
						tempNeuralNetwork.getIModel().destroyObject();
					}
				}	*/			
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		} 
		else {
			if (this.modelInterpreter.getSubmit().equals("PFA")) {
				try {
					neuralNetworkTrainer.buildNetwork();
					evaluation = new Evaluation(neuralNetworkTrainer.getTrainInstances());
					evaluation.crossValidateModel(neuralNetworkTrainer.getNetwork(), neuralNetworkTrainer.getTrainInstances(), 10,new Random(1));
					if(evaluation.rootMeanSquaredError()<this.modelInterpreter.getBestRootMeanSquaredError()){
						this.modelInterpreter.setBestIteration(indexToTrain);
						this.modelInterpreter.setBestRootMeanSquaredError(evaluation.rootRelativeSquaredError());
						this.modelInterpreter.setBestMLP(neuralNetworkTrainer.getNetwork());
						this.modelInterpreter.setEvaluation(evaluation);
						this.modelInterpreter.setTrainingInstances(neuralNetworkTrainer.getTrainInstances());
						neuralNetworkTrainer.setNodeNamesToWeightName(topology);
						this.modelInterpreter.setNeuralNetworkTopology(neuralNetworkTrainer.getTopology());
					}/*
					else{
						int index=this.modelInterpreter.getRoot().getRootModels().indexOf(tempNeuralNetwork);
						if(index!=-1){
							tempNeuralNetwork.getIModel().destroyObject();
						}
					}			*/
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		}
		
		int tempIteration=modelInterpreter.getIteration();
		tempIteration++;
		modelInterpreter.setIteration(tempIteration);
	}
	
}
