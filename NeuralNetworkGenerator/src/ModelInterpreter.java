//package neuralNetworkModelInterpreter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableCellEditor;
import javax.xml.bind.JAXBException;

import org.isis.gme.bon.BONComponent;
import org.isis.gme.bon.JBuilder;
import org.isis.gme.bon.JBuilderConnection;
import org.isis.gme.bon.JBuilderFolder;
import org.isis.gme.bon.JBuilderModel;
import org.isis.gme.bon.JBuilderObject;

import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;

public class ModelInterpreter implements BONComponent {

	List<TableCellEditor> editors = new ArrayList<TableCellEditor>(3);
	String locationOriginalDataFile;
	public String temp;
	JBuilderFolder root;
	JBuilderModel model = null;
	JBuilderModel neuralNetworkModel =null;
	double bestRootMeanSquaredError=1000;
	int bestIteration;
	MyMultilayerPerceptron bestMLP;
	Evaluation evaluation;
	String filetoSave="";
	ArrayList<Integer> numberNeuronChecked;
	NeuralNetworkTopology neuralNetworkTopology;
	String submit;
	ArrayList<Integer> potentialNumHiddenNeurons;
	int numHiddenNeurons;
	int numInputNeurons;
	String outputTypeFromData;
	Instances reducedDataSet;
	Instances trainingInstances;
	String filePathField;
	String learningRateField;
	String momentumField;
	String epochField;
	String sizeValidationField;
	String seedField;
	int iteration;
	ArrayList<JBuilderModel> tempNeuralNetworkArray;
	ArrayList<List<List<String>>> tempHiddenNeuronsPerLayer;
	ArrayList<HashMap<String, String>> tempTopology;
	ArrayList<NeuralNetworkConfiguration> listTrainer;
	ExecutorService executor;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void invokeEx(JBuilder builder, JBuilderObject focus,
			Collection selected, int param) {
		//Execute the interpreter only if a process model is open
		if (focus==null || focus.getKindName()==null || !focus.getKindName().equals("Process")) {
			JOptionPane.showMessageDialog(null, "Please open a Process model to generate neural network.", "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			//Verify is a metric is specify in the model
			model = (JBuilderModel) focus;
			Vector<JBuilderModel> metrics = model.getModels("Metric");
			if(metrics.isEmpty()){
				JOptionPane.showMessageDialog(null, "You did not specify a metric in your model.", "Error", JOptionPane.ERROR_MESSAGE );
			}
			else{
				submit = "";
				//Frame to collect the data, will update the attribute locationOriginalDataFile
				FrameForManufacturingModel frameForDataCollection = new FrameForManufacturingModel(this,model);
				frameForDataCollection.start();
				submit="";
				while (submit.equals("")) {
				}
				if (submit.equals("Submit")) {	
					DataPreProcessor datapreprocessor=new DataPreProcessor(locationOriginalDataFile, this);
					if (!submit.equals("Cancel")) {
						new MyProgressBarForDataPreprocessing(this,datapreprocessor).start();
					}
					//Pre-process the data. Update the attributes: potentialNumHiddenNeurons,numInputNeurons,
					//reducedDataSet, and outputTypeFromData	
					ArrayList<String> processModelProperties = new ArrayList<String>();
					Vector<JBuilderModel> property = model.getModels("Property");
					Iterator<JBuilderModel> iter = property.iterator();
					while (iter.hasNext()) {
						processModelProperties.add(iter.next().getName());
					}
					iter = metrics.iterator();
					JBuilderModel metric=iter.next();
					datapreprocessor.setMetric(metric);
					datapreprocessor.setProcessModelProperties(processModelProperties);
					//Save the originalFile for later purpose
					
					//progressBarDataPreprocess.setDataPreprocessor(datapreprocessor);
					if (submit.equals("Submit")) {	
						datapreprocessor.dataSetPreprocessing();
						// Save the reduced data obtained after the data pre-processing in csv and arff files
						try {
							String newData = "";
							for(int i = 0; i < reducedDataSet.numAttributes(); i++){
								if(i>0){
									newData += ",";
								}
								newData += reducedDataSet.attribute(i).name();
							}
							newData+="\n";
							for(int j = 0; j < reducedDataSet.numInstances(); j++){
									newData += reducedDataSet.instance(j);
								newData+="\n";
							}
							File originalDataFile=new File(locationOriginalDataFile);
							FileWriter fw = new FileWriter(originalDataFile.getParent() + "\\newData.csv");
							fw.write(newData);
							fw.close();
							fw = new FileWriter(originalDataFile.getParent() + "\\newData.arff");
							fw.write(reducedDataSet.toString());
							fw.close();
							filePathField=originalDataFile.getParent() + "\\newData.csv";
						} catch (Exception ex) {
							submit="Cancel";
							JOptionPane.showMessageDialog(null, "The pre-processed data have not been recorded. Program aborted", "Error", JOptionPane.ERROR_MESSAGE );
						}
					}
					if (submit.equals("Submit")) {
						submit = "";
						//Display the frame to configure the neural network training
						FrameForNeuralNetworkModel frameForTrainingConfiguration = new FrameForNeuralNetworkModel(this, "MultilayerPerceptron");
						frameForTrainingConfiguration.start();
						while (frameForTrainingConfiguration.isAlive()) {
						}
						iteration=0;
						numberNeuronChecked=new ArrayList<Integer>();
						if (!submit.equals("Cancel")) {
							new MyProgressBarForTraining(this).start();
						}
						if(!submit.equals("Cancel")){
							tempNeuralNetworkArray=new ArrayList<JBuilderModel>();
							tempHiddenNeuronsPerLayer=new ArrayList<List<List<String>>>();
							tempTopology=new ArrayList<HashMap<String,String>>();
							listTrainer=new ArrayList<NeuralNetworkConfiguration>();
							for(int k=potentialNumHiddenNeurons.size()-1;k>=0;k--){
	
								numHiddenNeurons=potentialNumHiddenNeurons.get(k);
								if(!numberNeuronChecked.contains(numHiddenNeurons)&&numHiddenNeurons>0){
									numberNeuronChecked.add(numHiddenNeurons);
									int layer=1;
									model = (JBuilderModel) focus;
									root=builder.getRootFolder();
									JBuilderModel tempNeuralNetworkModel= root.createNewModel("NeuralNetworkModel");
									tempNeuralNetworkModel.setName(this.model.getName()+"NeuralNetworkModel");
									tempNeuralNetworkModel.setAttribute("type", "MultilayerPerceptron");
									tempNeuralNetworkModel.setAttribute("numberOfHiddenLayers", 1);
									int spacingRows = 100;
									int spacingColumns = 250;
									int x=100;
									int y=spacingRows;
									// Create an array of Input Neurons from the attributes of the reduced data set
									JBuilderModel inputNeurons [] = new JBuilderModel [numInputNeurons];
									for(int i = 0 ; i < numInputNeurons;i++)
									{
										inputNeurons[i] = tempNeuralNetworkModel.createNewModel("InputNeuron");
										inputNeurons[i].setName(reducedDataSet.attribute(i).name());
										inputNeurons[i].setAttribute("layer", layer);
										
										setPosition(inputNeurons[i],(x+"," +y));
										y+=spacingRows;
									}
									// Create an array of Bias Neurons for the input layer and hidden layer(s)
									JBuilderModel biasNeurons [] = new JBuilderModel [2];
									biasNeurons[0] = tempNeuralNetworkModel.createNewModel("BiasNeuron");
									biasNeurons[0].setName("BiasNeuron1");
									biasNeurons[0].setAttribute("layer", layer);				
									setPosition(biasNeurons[0],(x+","+y));
									layer++;
									x+=spacingColumns;
									//test/
									double ratio=(double)numHiddenNeurons/(double)numInputNeurons;
									if(ratio>1){
										y=spacingRows-(int)(spacingRows*(ratio-1));
									}
									else{
										y=spacingRows;
									}
									//  Create an array of Hidden Neurons from the number of Hidden Neurons suggested to use from the Attribute Selection
									JBuilderModel hiddenNeurons [] = new JBuilderModel [numHiddenNeurons];
									for(int i = 0 ; i < numHiddenNeurons;i++)
									{
										hiddenNeurons[i] = tempNeuralNetworkModel.createNewModel("HiddenNeuron");
										hiddenNeurons[i].setName("HiddenNeuron" + (i+1));
										hiddenNeurons[i].setAttribute("layer", layer);
										setPosition(hiddenNeurons[i],(x +"," + y));
										y+=spacingRows;
									}
									biasNeurons[1] = tempNeuralNetworkModel.createNewModel("BiasNeuron");
									biasNeurons[1].setName("BiasNeuron2");
									biasNeurons[1].setAttribute("layer", layer);
									setPosition(biasNeurons[1],(x+"," +y));
									layer++;
									x+=spacingColumns;
									y=spacingRows*numInputNeurons/2;
									// Create an Output Neuron with the target of analysis
									JBuilderModel outputNeuron = tempNeuralNetworkModel.createNewModel("OutputNeuron");
									outputNeuron.setName(outputTypeFromData);
									outputNeuron.setAttribute("layer", layer);
									setPosition(outputNeuron,x +"," + y);
					
									//Create connections between Neurons
									int visibleEdgeCounter = 0; //counter used to keep track of visible edge names
									int hiddenEdgeCounter = 0; //counter used to keep track of hidden edge names
					
									// Create an array of connections from each input neuron (source) to each hidden neuron (destination)
									JBuilderConnection inputToHidden [] = new JBuilderConnection [numHiddenNeurons*numInputNeurons];
									for(int i = 0 ; i < numInputNeurons;i++)
									{
										for(int j = 0 ; j < numHiddenNeurons;j++){
											inputToHidden[i] = tempNeuralNetworkModel.createNewConnection("VisibleEdge", inputNeurons[i], hiddenNeurons[j]);
											visibleEdgeCounter++;
											inputToHidden[i].setName("VisibleEdge" + visibleEdgeCounter);
											setConnection(inputToHidden[i],("Ew"));
										}
									}
									// Create an array of connections from each bias neuron (source) to each hidden neuron (destination)
									JBuilderConnection biasToHidden [] = new JBuilderConnection [numHiddenNeurons];
									for(int i = 0 ; i < numHiddenNeurons;i++){
										biasToHidden[i] = tempNeuralNetworkModel.createNewConnection("HiddenEdge", biasNeurons[0], hiddenNeurons[i]);
										hiddenEdgeCounter++;
										biasToHidden[i].setName("HiddenEdge" + hiddenEdgeCounter);
										setConnection(biasToHidden[i],("Ew"));
									}
									
									visibleEdgeCounter++;
									// Create a connection from bias neuron 2 (source) to the output neuron (destination)
									JBuilderConnection biasToOutput = tempNeuralNetworkModel.createNewConnection("VisibleEdge", biasNeurons[1], outputNeuron);
									biasToOutput.setName("VisibleEdge" + visibleEdgeCounter);
									setConnection(biasToOutput,("Ew"));
									
									// Create an array of connections from each hidden neuron (source) to the output neuron (destination)
									JBuilderConnection hiddenToOutput [] = new JBuilderConnection [numHiddenNeurons];
									for(int i = 0 ; i < numHiddenNeurons;i++){
										hiddenToOutput[i] = tempNeuralNetworkModel.createNewConnection("VisibleEdge", hiddenNeurons[i], outputNeuron);
										visibleEdgeCounter++;
										hiddenToOutput[i].setName("VisibleEdge" + visibleEdgeCounter);
										setConnection(hiddenToOutput[i],("Ew"));
									}
									tempNeuralNetworkArray.add(tempNeuralNetworkModel);
									getHiddenNeuronsPerLayer(tempNeuralNetworkArray.size()-1);
									getEdgesList(tempNeuralNetworkArray.size()-1);
								}
								else{
									iteration++;
								}
							}
							int processornumber=Runtime.getRuntime().availableProcessors();
							executor = Executors.newFixedThreadPool(processornumber-1);
							for(int i=0;i<tempNeuralNetworkArray.size();i++){
								if(!submit.equals("Cancel")){
									listTrainer.add(new NeuralNetworkConfiguration(this, i));
									executor.execute(listTrainer.get(i));
								}
							}
						}
					}
					if(!submit.equals("Cancel")){
						while(iteration<potentialNumHiddenNeurons.size()&&listTrainer!=null&&!listTrainer.isEmpty()){
							if(submit.equals("Cancel")){
								executor.shutdownNow();
								for (int i=0;i<tempNeuralNetworkArray.size();i++){
									int index=root.getRootModels().indexOf(tempNeuralNetworkArray.get(i));
									if(index!=-1&&root.getRootModels().get(i)!=null){
										((JBuilderModel)root.getRootModels().get(index)).getIModel().destroyObject();
										temp="";
									}
								}
								iteration=potentialNumHiddenNeurons.size();
							}
						}
						if(executor!=null){
							executor.shutdown();
						}
					}
					if(submit.equals("Cancel")){
						if(neuralNetworkModel!=null){
							neuralNetworkModel.getIModel().destroyObject();
						}
					}
					else{
						int i=0;
						model=tempNeuralNetworkArray.get(bestIteration);
						while(i<tempNeuralNetworkArray.size()){
							if(tempNeuralNetworkArray.get(i).getIModel()!=null){
								tempNeuralNetworkArray.get(i).getIModel().destroyObject();
							}
							i++;
						}
					//	backTrackOnBiasNeuronsEdges(neuralNetworkTopology.getBiasNameToValue());
					//	backTrackOnNeuronsEdges(neuralNetworkTopology.getWeightNameToValue());
						if (submit.equals("PMML")) {
							NeuralNetworkWriter writer=new NeuralNetworkWriter(bestMLP, reducedDataSet);
							try {
								//JOptionPane.showMessageDialog(null, evaluation.toSummaryString(), "Information about the selected neural network", JOptionPane.INFORMATION_MESSAGE);
								String tem="";
								for(int p=0;p<reducedDataSet.size();p++){
									try {
										tem+=reducedDataSet.get(p).value(reducedDataSet.numAttributes()-1)+","+bestMLP.distributionForInstance(reducedDataSet.get(p))[0]+"\n";
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								FileWriter fw;
								try {
									File originalDataFile=new File(locationOriginalDataFile);
									fw = new FileWriter(originalDataFile.getParent() + "\\predictions.csv");
									fw.write(tem);
									fw.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								filetoSave=writer.networkToPMML(trainingInstances);
							} catch (JAXBException e) {
								JOptionPane.showMessageDialog(null, e.getMessage());
							}
							JFileChooser fileChooser = new JFileChooser();
							FileNameExtensionFilter ft = new FileNameExtensionFilter( "PMML Files", "pmml" );
							fileChooser.addChoosableFileFilter(ft); fileChooser.setFileFilter(ft);
							int returnVal = fileChooser.showSaveDialog(null);
							if (returnVal == JFileChooser.APPROVE_OPTION) {
								try {
									String fileName = fileChooser.getSelectedFile().getAbsolutePath();
									if(fileName.lastIndexOf('.') == -1 ) 
										fileName += ".pmml";
									FileWriter fw = new FileWriter(fileName);
									//FileWriter fw = new FileWriter(fileChooser
									//		.getSelectedFile() + ".pmml");
									fw.write(filetoSave);
									fw.close();
								} catch (Exception ex) {
									JOptionPane.showMessageDialog(null, "Error "+ex.getMessage());
									ex.printStackTrace();
								}
							}
						}
						else {
							if (submit.equals("PFA")) {
								if (canCallPython()) {
									if (pythonHasTitus()) {
										try {
											NeuralNetworkWriter writer=new NeuralNetworkWriter(bestMLP, reducedDataSet);
											filetoSave=writer.networkToPFA();
											// Start a process that will execute the Python module:
											Path path = Paths.get(NeuralNetworkTrainer.class.getProtectionDomain().getCodeSource().getLocation().toURI());
											String m_pathToPythonModule=path.toString()+"\\mlp_to_PFA.py";
											ProcessBuilder pb = new ProcessBuilder("python",m_pathToPythonModule, filetoSave);
											// Start the process and read its output:
											Process p = pb.start();
											BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
											StringWriter sr = new StringWriter();
											int buff_size = 16384;
											char[] buff = new char[buff_size];
											int n = -1;
											while ((n = in.read(buff)) >= 0) {
												sr.write(buff, 0, n);
											}
											in.close();
				
											filetoSave = sr.toString();
				
										} catch (Exception e) {
				
											JOptionPane.showMessageDialog(null, e.getMessage());
										}
										JFileChooser fileChooser = new JFileChooser();
										FileNameExtensionFilter ft = new FileNameExtensionFilter( "PFA Files", "pfa" );
										fileChooser.addChoosableFileFilter(ft); fileChooser.setFileFilter(ft);
										int returnVal = fileChooser.showSaveDialog(null);
										if (returnVal == JFileChooser.APPROVE_OPTION) {
											try {
												String fileName = fileChooser.getSelectedFile().getAbsolutePath();
												if(fileName.lastIndexOf('.') == -1 ) 
													fileName += ".pfa";
												FileWriter fw = new FileWriter(fileName);
												//FileWriter fw = new FileWriter(fileChooser
												//		.getSelectedFile() + ".pfa");
												fw.write(filetoSave);
												fw.close();
											} catch (Exception ex) {
												ex.printStackTrace();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void getHiddenNeuronsPerLayer(int index) {
		List<List<String>> hiddenNeuronsPerLayer;
		int hiddenLayerNumber = Integer.parseInt(tempNeuralNetworkArray.get(index).getIModel()
				.getStrAttrByName("numberOfHiddenLayers"));
		hiddenNeuronsPerLayer = new ArrayList<List<String>>();
		for (int i = 0; i < hiddenLayerNumber; i++) {
			hiddenNeuronsPerLayer.add(new ArrayList<String>());
		}
		Vector<?> hiddenNeuronVector = tempNeuralNetworkArray.get(index).getModels("HiddenNeuron");
		Iterator<?> hiddenNeuronsIterator = hiddenNeuronVector.iterator();
		while (hiddenNeuronsIterator.hasNext()) {
			JBuilderModel hiddenNeuron = ((JBuilderModel) hiddenNeuronsIterator
					.next());
			int layerNumber = Integer.parseInt(hiddenNeuron.getIModel()
					.getStrAttrByName("layer"));
			hiddenNeuronsPerLayer.get(layerNumber - 2).add(
					hiddenNeuron.getName());
		}
		tempHiddenNeuronsPerLayer.add(hiddenNeuronsPerLayer);
	}

	private void getEdgesList(int index) {
		HashMap<String, String> topology;
		topology = new HashMap<String, String>();
		Vector<?> modelsVector = tempNeuralNetworkArray.get(index).getChildren();
		Iterator<?> objects = modelsVector.iterator();
		while (objects.hasNext()) {
			JBuilderObject object = ((JBuilderObject) objects.next());
			if (object instanceof JBuilderConnection) {
				JBuilderConnection connection = ((JBuilderConnection) object);
				topology.put(connection.getSource().getName()
						+ connection.getDestination().getName(),
						connection.getName());
			}
		}
		tempTopology.add(topology);
	}

	private void backTrackOnBiasNeuronsEdges(Map<String, Double> biasEdges) {
		Vector<?> biasNeuronsVector = model.getModels("BiasNeuron");
		Iterator<?> biasNeuronsIterator = biasNeuronsVector.iterator();
		while (biasNeuronsIterator.hasNext()) {
			JBuilderModel biasNeuron = ((JBuilderModel) biasNeuronsIterator
					.next());
			Vector<JBuilderConnection> outConnections = biasNeuron
					.getOutConnections("VisibleEdge");
			Iterator<JBuilderConnection> connections = outConnections
					.iterator();
			while (connections.hasNext()) {
				JBuilderConnection connection = connections.next();
				if (biasEdges.get(((JBuilderModel) connection.getDestination())
						.getName()) != null) {
					String temp = (biasEdges.get(((JBuilderModel) connection
							.getDestination()).getName())).toString();
					connection.getIConnection()
							.setStrAttrByName("weight", temp);
				}
			}
			outConnections = biasNeuron.getOutConnections("HiddenEdge");
			connections = outConnections.iterator();
			while (connections.hasNext()) {
				JBuilderConnection connection = connections.next();
				if (biasEdges.get(((JBuilderModel) connection.getDestination())
						.getName()) != null) {
					String temp = (biasEdges.get(((JBuilderModel) connection
							.getDestination()).getName())).toString();
					connection.getIConnection()
							.setStrAttrByName("weight", temp);
				}
			}
		}
	}

	private void backTrackOnNeuronsEdges(Map<String, Double> neuronsEdges) {
		Vector<JBuilderConnection> outConnections = model
				.getConnections("VisibleEdge");
		Iterator<JBuilderConnection> connections = outConnections.iterator();
		while (connections.hasNext()) {
			JBuilderConnection connection = connections.next();

			if (neuronsEdges.get(connection.getName()) != null) {
				String temp = neuronsEdges.get(connection.getName()).toString();
				connection.getIConnection().setStrAttrByName("weight", temp);
			}
		}
		outConnections = model.getConnections("HiddenEdge");
		connections = outConnections.iterator();
		while (connections.hasNext()) {
			JBuilderConnection connection = connections.next();
			if (neuronsEdges.get(connection.getName()) != null) {
				String temp = neuronsEdges.get(connection.getName()).toString();
				connection.getIConnection().setStrAttrByName("weight", temp);
			}
		}
	}
	
	/**
	 * Tests if Python has the Titus library installed by creating a process and
	 * executing a Python program that only attempts to import Titus and print
	 * something that can be read back.
	 * 
	 * @return true if the test succeeds and false otherwise.
	 */
	public boolean pythonHasTitus() {
		// Call the Python module as an OS process
		String ret = null;
		String getsPrinted = "1";

		try {

			String prg = "import titus\nprint " + getsPrinted + "\n";
			BufferedWriter out = new BufferedWriter(new FileWriter("test1.py"));
			out.write(prg);
			out.close();

			ProcessBuilder pb = new ProcessBuilder("python", "test1.py");
			Process p = pb.start();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			ret = in.readLine();

		} catch (Exception e) {

			System.out.println(e);
		}

		return ret.equals(getsPrinted);
	}

	/**
	 * Tests is it is possible to create a process, create and execute a trivial
	 * Python program inside the process, and get back the correct result.
	 * 
	 * @return true if the test succeeds and false otherwise.
	 */
	public boolean canCallPython() {
		int ret = 0;
		int number1 = 10;
		int number2 = 32;

		try {

			String prg = "import sys\nprint int(sys.argv[1])+int(sys.argv[2])\n";
			BufferedWriter out = new BufferedWriter(new FileWriter("test1.py"));
			out.write(prg);
			out.close();

			ProcessBuilder pb = new ProcessBuilder("python", "test1.py", ""
					+ number1, "" + number2);
			Process p = pb.start();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			ret = new Integer(in.readLine()).intValue();

		} catch (Exception e) {

			e.getMessage();
		}
		return ret == (number1 + number2);
	}
	
    private static void setPosition(JBuilderModel gmeObj, String position) {
        if(position == null) return;
        
        gmeObj.getIObject().setRegistryValue("PartRegs/Aspect/Position", position);
	}
	
    private static void setConnection(JBuilderConnection gmeObj, String position) {
        if(position == null) return;
        
        gmeObj.getIObject().setRegistryValue("autorouterPref", position);
	}
	@Override
	public void registerCustomClasses() {

	}

	public synchronized void setNeuralNetworkTopology(
			NeuralNetworkTopology neuralNetworkTopology) {
		this.neuralNetworkTopology = neuralNetworkTopology;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}
	
	public void setPotentialNumHiddenNeurons(ArrayList<Integer> numHiddenNeurons) {
		this.potentialNumHiddenNeurons = numHiddenNeurons;
	}

	public void setNumInputNeurons(int numInputNeurons) {
		this.numInputNeurons = numInputNeurons;
	}
	
	public int getNumInputNeurons() {
		return numInputNeurons;
	}

	public void setOutputType(String outputTypeFromData) {
		this.outputTypeFromData = outputTypeFromData;
	}
	
	public void setReducedDataSet(Instances reducedDataSet) {
		this.reducedDataSet = reducedDataSet;
	}
	public Instances getReducedDataSet() {
		return reducedDataSet;
	}
	
	public String getOutputTypeFromData() {
		return outputTypeFromData;
	}
	
	public String getFilePathField() {
		return filePathField;
	}

	public void setFilePathField(String filePathField) {
		this.filePathField = filePathField;
	}

	public String getLearningRateField() {
		return learningRateField;
	}

	public void setLearningRateField(String learningRateField) {
		this.learningRateField = learningRateField;
	}

	public String getMomentumField() {
		return momentumField;
	}

	public void setMomentumField(String momentumField) {
		this.momentumField = momentumField;
	}

	public String getEpochField() {
		return epochField;
	}

	public void setEpochField(String epochField) {
		this.epochField = epochField;
	}

	public String getSizeValidationField() {
		return sizeValidationField;
	}

	public void setSizeValidationField(String sizeValidationField) {
		this.sizeValidationField = sizeValidationField;
	}

	public String getSeedField() {
		return seedField;
	}

	public void setSeedField(String seedField) {
		this.seedField = seedField;
	}
	
	public synchronized int getIteration() {
		return iteration;
	}
	
	public synchronized void setIteration(int iteration) {
		this.iteration=iteration;
	}
	
	public synchronized ArrayList<Integer> getPotentialNumHiddenNeurons() {
		return potentialNumHiddenNeurons;
	}

	public synchronized double getBestRootMeanSquaredError() {
		return bestRootMeanSquaredError;
	}

	public synchronized void setBestRootMeanSquaredError(double bestRootMeanSquaredError) {
		this.bestRootMeanSquaredError = bestRootMeanSquaredError;
	}
	
	public void setTemp(String temp){
		this.temp=temp;
	}

	public ArrayList<JBuilderModel> getTempNeuralNetworkArray() {
		return tempNeuralNetworkArray;
	}

	public ArrayList<List<List<String>>> getTempHiddenNeuronsPerLayer() {
		return tempHiddenNeuronsPerLayer;
	}

	public ArrayList<HashMap<String, String>> getTempTopology() {
		return tempTopology;
	}

	public String getSubmit() {
		return submit;
	}

	public void setFiletoSave(String filetoSave) {
		this.filetoSave = filetoSave;
	}

	public synchronized void setBestIteration(int bestIteration) {
		this.bestIteration = bestIteration;
	}

	public synchronized void setBestMLP(MyMultilayerPerceptron bestMLP) {
		this.bestMLP = bestMLP;
	}
	
	public synchronized void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}
	
	public void setLocationOriginalDataFile(String locationOriginalDataFile) {
		this.locationOriginalDataFile = locationOriginalDataFile;
	}
	
	public synchronized void setTrainingInstances(Instances trainInstances){
		this.trainingInstances=trainInstances;
	}
	
}
