//package neuralNetworkModelInterpreter;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class FrameForNeuralNetworkModel extends Thread {

	static Dimension dimFrame = new Dimension(600, 300);
	JTextField filePathField;
	JTextField learningRateField;
	JTextField momentumField;
	JTextField epochField;
	JTextField sizeValidationField;
	JTextField seedField;
	ModelInterpreter modelInterpreter;
	String type;
	JFrame frame;

	public FrameForNeuralNetworkModel(ModelInterpreter modelInterpreter,
			String type) {
		super();
		this.modelInterpreter = modelInterpreter;
		this.type = type;
	}

	@Override
	public void run() {
		frameConfiguration(type);
		while (frame.isVisible()) {

		}
	}

	public void frameConfiguration(String type) {
		frame = new JFrame(type);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setSize(dimFrame);
		frame.setPreferredSize(dimFrame);
		Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((screen.width - frame.getWidth()) / 2,
				(screen.height - frame.getHeight()) / 2);
		configureNorthPanel(frame);
		configureCenterPanel(frame, type);
		configureSouthPanel(frame);
		frame.repaint();
		frame.validate();
		frame.setVisible(true);
	}

	private void configureNorthPanel(JFrame frame) {
		JPanel panel = new JPanel();
		String text = "Please configure your neural network for training:";
		JLabel label = new JLabel(text);
		panel.add(label);
		frame.add(panel, BorderLayout.NORTH);
		frame.repaint();
		frame.validate();
	}

	private void configureCenterPanel(final JFrame frame, String type) {
		double width = dimFrame.width * 0.8;
		double height = dimFrame.height * 0.65;
		JPanel mainPanel = new JPanel();
		JPanel optionsPanel = new JPanel();
		optionsPanel.setPreferredSize(new Dimension((int) width, 20));
		configureOptionsPanel(optionsPanel);
		JScrollPane scrollPane = new JScrollPane(optionsPanel);
		scrollPane.setPreferredSize(new Dimension((int) width, (int) height));
		scrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		mainPanel.add(scrollPane);
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.repaint();
		frame.validate();
	}

	private void configureSouthPanel(final JFrame frame) {
		JPanel panel = new JPanel();
		JButton pmmlButton = new JButton("Export in PMML");
		pmmlButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					modelInterpreter.setLearningRateField(learningRateField.getText());
					modelInterpreter.setMomentumField(momentumField.getText());
					modelInterpreter.setEpochField(epochField.getText());
					modelInterpreter.setSizeValidationField(sizeValidationField.getText());
					modelInterpreter.setSeedField(seedField.getText());
					modelInterpreter.setSubmit("PMML");
					frame.dispose();
			}

		});
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modelInterpreter.setSubmit("Cancel");
				frame.dispose();
			}
		});
		panel.add(pmmlButton);
		panel.add(cancelButton);
		frame.add(panel, BorderLayout.SOUTH);
		frame.repaint();
		frame.validate();
	}

	private void configureOptionsPanel(JPanel optionsPanel) {
		JPanel panelOption1 = new JPanel();
		JLabel labelOption1 = new JLabel(
				"Learning rate (between 0 and 1, if blank==>0.3):");
		learningRateField = new JTextField();
		learningRateField.setPreferredSize(new Dimension(100, 24));
		learningRateField.setText("0.3");
		panelOption1.add(labelOption1);
		panelOption1.add(learningRateField);
		optionsPanel.add(panelOption1);
		JPanel panelOption2 = new JPanel();
		JLabel labelOption2 = new JLabel(
				"Momentum (between 0 and 1, if blank==>0.2):");
		momentumField = new JTextField();
		momentumField.setPreferredSize(new Dimension(100, 24));
		momentumField.setText("0.2");
		panelOption2.add(labelOption2);
		panelOption2.add(momentumField);
		optionsPanel.add(panelOption2);
		JPanel panelOption3 = new JPanel();
		JLabel labelOption3 = new JLabel("Number of epochs (if blank==>500):");
		epochField = new JTextField();
		epochField.setPreferredSize(new Dimension(100, 24));
		epochField.setText("500");
		panelOption3.add(labelOption3);
		panelOption3.add(epochField);
		optionsPanel.add(panelOption3);
		JPanel panelOption4 = new JPanel();
		JLabel labelOption4 = new JLabel(
				"Percentage size of validation (use to terminate training, if blank==>0:");
		sizeValidationField = new JTextField();
		sizeValidationField.setPreferredSize(new Dimension(50, 24));
		sizeValidationField.setText("0");
		panelOption4.add(labelOption4);
		panelOption4.add(sizeValidationField);
		optionsPanel.add(panelOption4);
		JPanel panelOption5 = new JPanel();
		JLabel labelOption5 = new JLabel(
				"Seed the random number generator(>=0, if blank==>0):");
		seedField = new JTextField();
		seedField.setPreferredSize(new Dimension(100, 24));
		seedField.setText("0");
		panelOption5.add(labelOption5);
		panelOption5.add(seedField);
		optionsPanel.add(panelOption5);
	}

	public JTextField getFilePathField() {
		return filePathField;
	}

	public JTextField getLearningRateField() {
		return learningRateField;
	}

	public JTextField getMomentumField() {
		return momentumField;
	}

	public JTextField getEpochField() {
		return epochField;
	}

	public JTextField getSizeValidationField() {
		return sizeValidationField;
	}

	public JTextField getSeedField() {
		return seedField;
	}
}
