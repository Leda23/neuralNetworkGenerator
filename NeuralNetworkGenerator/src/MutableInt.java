//package neuralNetworkModelInterpreter;

public class MutableInt {

	int m_value;

	MutableInt() {
		m_value = 0;
	}

	MutableInt(int value) {
		m_value = value;
	}

	@Override
	public boolean equals(Object other) {

		if (!(other instanceof MutableInt)) {
			return false;
		}

		if (this == other) {
			return true;
		}

		MutableInt otherMutableInt = (MutableInt) other;

		return m_value == otherMutableInt.m_value;
	}

	public void incr() {
		++m_value;
	}

	public void plus(MutableInt other) {
		m_value += other.m_value;
	}

	public void plus(int value) {
		m_value += value;
	}

	public int getValue() {
		return m_value;
	}

	public void setValue(int value) {
		m_value = value;
	}
}
