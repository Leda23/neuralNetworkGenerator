/* Author: Steven Hudak (NIST - srh1@nist.gov)
 * 
 * This file contains the class NeuralNetworkStandard which is used for
 * building, training, and evaluating a network, and for constructing
 * Predictive Model Markup Language (PMML) and Portable Format for Analytics
 * (PFA) descriptions of the network.
 */

//package neuralNetworkModelInterpreter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * A class representing a multilayer perceptron trained with backpropagation.
 * The hidden nodes all use a sigmoid activation function, and the output nodes
 * use sigmoid for classification and unthresholded linear for regression.
 * 
 * @author Steven Hudak (NIST - srh1@nist.gov)
 */
public class NeuralNetworkTrainer{

	/**
	 * Each row is a list of the names of all hidden nodes in a specific layer.
	 * The order is increasing index goes from first hidden layer to 2nd ... and
	 * so on.
	 */
	List<List<String>> m_hiddenLayerNames;

	/**
	 * Each string indicates the number of hidden nodes in the corresponding
	 * layer. The order is increasing index goes from first hidden layer to 2nd
	 * ... and so on.
	 */
	List<String> m_hiddenLayers;

	/**
	 * A mapping from the concatenation of the source node's name with the
	 * destination node's name to the corresponding weight's name.
	 */
	Map<String, String> m_nodeNamesToWeightName;

	/**
	 * Learning Rate for the backpropagation algorithm. (Value should be between
	 * 0 - 1, Default = 0.3).
	 */
	String m_learningRate;

	/**
	 * Momentum Rate for the backpropagation algorithm. (Value should be between
	 * 0 - 1, Default = 0.2)
	 */
	String m_momentum;

	/**
	 * Number of epochs to train through. (Default = 500).
	 */
	String m_numEpochs;

	/**
	 * Percentage size of validation set to use to terminate training (if this
	 * is non zero it can preempt num of epochs. (Value should be between 0 -
	 * 100, Default = 0).
	 */
	String m_percentValidation;

	/**
	 * The value used to seed the random number generator (Value should be &gt;=
	 * 0 and and a long, Default = 0).
	 */
	String m_seed;

	/**
	 * The name of the file that holds the data used to train the network via
	 * backpropagation.
	 */
	String m_trainingDataFilename;

	/**
	 * A collection of all the options that will be used to build and train the
	 * network.
	 */
	String[] m_options;

	/**
	 * All of the training data in a structured form.
	 */
	Instances m_trainInstances;

	/**
	 * The neural network.
	 */
	MyMultilayerPerceptron m_network;


	/**
	 * Default constructor does nothing. The training data filename must be set
	 * before a network can be built.
	 */
	public NeuralNetworkTrainer() {
		
	}

	/**
	 * Constructor using all the default values for parameters.
	 * 
	 * @param trainingDataFilename
	 *            The name of the file that holds the data that will be used to
	 *            train the neural network.
	 * @throws Exception
	 */
	public NeuralNetworkTrainer(String trainingDataFilename) throws Exception {
		m_trainingDataFilename = trainingDataFilename;
		loadTrainingData(m_trainingDataFilename);
	}

	/**
	 * Data is retrieved from the file @param trainingDataFilename, converted to
	 * and Instances object, and stored in m_trainInstances.
	 * 
	 * @param trainingDataFilename
	 *            The name of the file that holds the data that will be used to
	 *            train the neural network.
	 * @throws Exception
	 */
	private void loadTrainingData(String trainingDataFilename) throws Exception {
		DataSource source = new DataSource(m_trainingDataFilename);
		// Import data to a structured form (Instances) and select the column
		// that is to be predicted:
		m_trainInstances = source.getDataSet();
		m_trainInstances.setClassIndex(m_trainInstances.numAttributes() - 1);
		/*NominalToBinary filter=new NominalToBinary();
		filter.setTransformAllValues(true);
		filter.setInputFormat(m_trainInstances);
		m_trainInstances = Filter.useFilter(m_trainInstances, filter);*/
	}
	
	/**
	 * Constructor with inputs using their natural data type.
	 * 
	 * @param hiddenLayerNames
	 *            Each row contains the names of the neurons in the
	 *            corresponding hidden layer.
	 * @param nodeNamesToWeightName
	 *            A mapping from "source node name" + "destination node name" to
	 *            "edge name".
	 * @param learningRate
	 *            Learning Rate for the backpropagation algorithm. (Value should
	 *            be between 0 - 1, Default = 0.3).
	 * @param momentum
	 *            Momentum Rate for the backpropagation algorithm. (Value should
	 *            be between 0 - 1, Default = 0.2).
	 * @param numEpochs
	 *            Number of epochs to train through. (Default = 500).
	 * @param percentValidation
	 *            Percentage size of validation set to use to terminate training
	 *            (if this is non zero it can preempt num of epochs. (Value
	 *            should be between 0 - 100, Default = 0).
	 * @param seed
	 *            The value used to seed the random number generator (Value
	 *            should be &gt;= 0 and and a long, Default = 0).
	 * @param trainingDataFilenames
	 *            The name of the file that holds the data that will be used to
	 *            train the neural network.
	 * @throws Exception
	 */
	public NeuralNetworkTrainer(List<List<String>> hiddenLayerNames,
			HashMap<String, String> nodeNamesToWeightName, double learningRate,
			double momentum, int numEpochs, int percentValidation, double seed,
			String trainingDataFilename)
			throws Exception {

		m_hiddenLayerNames = hiddenLayerNames;
		m_hiddenLayers = layerNamesToLayerCounts(hiddenLayerNames);
		m_learningRate = Double.toString(learningRate);
		m_momentum = Double.toString(momentum);
		m_numEpochs = Integer.toString(numEpochs);
		m_percentValidation = Integer.toString(percentValidation);
		m_seed = Double.toString(seed);
		m_trainingDataFilename = trainingDataFilename;
		loadTrainingData(m_trainingDataFilename);
	}

	/**
	 * Constructor with all inputs as strings.
	 * 
	 * @param hiddenLayerNames
	 *            Each row contains the names of the neurons in the
	 *            corresponding hidden layer.
	 * @param nodeNamesToWeightName
	 *            A mapping from "source node name" + "destination node name" to
	 *            "edge name".
	 * @param learningRate
	 *            Learning Rate for the backpropagation algorithm. (Value should
	 *            be between 0 - 1, Default = 0.3).
	 * @param momentum
	 *            Momentum Rate for the backpropagation algorithm. (Value should
	 *            be between 0 - 1, Default = 0.2).
	 * @param numEpochs
	 *            Number of epochs to train through. (Default = 500).
	 * @param percentValidation
	 *            Percentage size of validation set to use to terminate training
	 *            (if this is non zero it can preempt num of epochs. (Value
	 *            should be between 0 - 100, Default = 0).
	 * @param seed
	 *            The value used to seed the random number generator (Value
	 *            should be &gt;= 0 and and a long, Default = 0).
	 * @param trainingDataFilenames
	 *            The name of the file that holds the data that will be used to
	 *            train the neural network.
	 * @throws Exception
	 */
	public NeuralNetworkTrainer(List<List<String>> hiddenLayerNames,
			HashMap<String, String> nodeNamesToWeightName, String learningRate,
			String momentum, String numEpochs, String percentValidation,
			String seed, String trainingDataFilename)
			throws Exception {
		m_hiddenLayerNames = hiddenLayerNames;
		m_hiddenLayers = layerNamesToLayerCounts(hiddenLayerNames);
		m_learningRate = learningRate;
		m_momentum = momentum;
		m_numEpochs = numEpochs;
		m_percentValidation = percentValidation;
		m_seed = seed;
		m_trainingDataFilename = trainingDataFilename;
		loadTrainingData(m_trainingDataFilename);
	}

	/**
	 * Constructs from the individually specified options, the list of options
	 * that will be used to construct the multilayer perceptron.
	 */
	public void setOptions() {
		ArrayList<String> result = new ArrayList<>();

		if (m_hiddenLayers != null) {
			String temp = "";

			result.add("-H");

			for (int i = 0; i < m_hiddenLayers.size(); ++i) {
				temp += m_hiddenLayers.get(i) + ",";
			}
			temp = temp.substring(0, temp.length() - 1);
			result.add(temp);
		}
		if (m_learningRate != null) {
			result.add("-L");
			result.add(m_learningRate);
		}
		if (m_momentum != null) {
			result.add("-M");
			result.add(m_momentum);
		}
		if (m_numEpochs != null) {
			result.add("-N");
			result.add(m_numEpochs);
		}
		if (m_percentValidation != null) {
			result.add("-V");
			result.add(m_percentValidation);
		}
		if (m_seed != null) {
			result.add("-S");
			result.add(m_seed);
		}

		m_options = result.toArray(new String[result.size()]);
	}

	/**
	 * Constructs a multilayer perceptron using the training data,
	 * m_trianInstances, and the options specified by m_options. The resulting
	 * network is stored in m_multilayerPerceptron.
	 * 
	 * @throws Exception
	 */
	public void buildNetwork() throws Exception {
		setOptions();
		m_network = new MyMultilayerPerceptron();
		m_network.setOptions(m_options);
		m_network.buildClassifier(m_trainInstances);
		
	}


	/**
	 * @return A class holding two mappings: one from a node's name to its bias
	 *         value, the other from a weight's name to its value.
	 */
	public NeuralNetworkTopology getTopology() {
		return m_network.getTopology(m_hiddenLayerNames,
				m_nodeNamesToWeightName);
	}

	/**
	 * Evaluates the multilayer perceptron, m_multilayerPerceptron, on the
	 * training instances, m_trainInstances.
	 * 
	 * @return A summary of the results of the evaluation.
	 * @throws Exception
	 */
	public String evaluateNetwork() throws Exception {
		if (m_network != null) {
			Evaluation evaluation = new Evaluation(m_trainInstances);
			evaluation.crossValidateModel(m_network, m_trainInstances, 10,
					new Random(1));
			return evaluation.toSummaryString();
		} else {
			return "The multilayer perceptron has not been built."
					+ " Try the buildNetwork() method";
		}
	}

	

	/**
	 * Counts the length of the rows in @param layerNames
	 * 
	 * @param layerNames
	 * @return A list of the lengths converted to strings.
	 */
	public static List<String> layerNamesToLayerCounts(
			List<List<String>> layerNames) {
		List<String> result = new ArrayList<>();

		for (int i = 0; i < layerNames.size(); ++i) {
			List<String> neuronNames = layerNames.get(i);

			result.add(Integer.toString(neuronNames.size()));
		}
		return result;
	}

	// GETTERS AND SETTERS: //

	public List<List<String>> getHiddenLayerNames() {
		return m_hiddenLayerNames;
	}

	public void setHiddenLayerNames(List<List<String>> m_hiddenLayerNames) {
		this.m_hiddenLayerNames = m_hiddenLayerNames;
		m_hiddenLayers = layerNamesToLayerCounts(this.m_hiddenLayerNames);
	}

	public List<String> getHiddenLayers() {
		return m_hiddenLayers;
	}

	public void setHiddenLayers(List<String> m_hiddenLayers) {
		this.m_hiddenLayers = m_hiddenLayers;
	}

	public void setHiddenLayers(String[] hiddenLayers) {
		List<String> result = new ArrayList<>();

		for (int i = 0; i < hiddenLayers.length; ++i) {
			result.add(hiddenLayers[i]);
		}
		m_hiddenLayers = result;
	}

	public Map<String, String> getNodeNamesToWeightName() {
		return m_nodeNamesToWeightName;
	}

	public void setNodeNamesToWeightName(
			Map<String, String> nodeNamesToWeightName) {
		m_nodeNamesToWeightName = nodeNamesToWeightName;
	}

	public String getLearningRate() {
		return m_learningRate;
	}

	public void setLearningRate(String m_learningRate) {
		this.m_learningRate = m_learningRate;
	}

	public void setLearningRate(double m_learningRate) {
		this.m_learningRate = Double.toString(m_learningRate);
	}

	public String getMomentum() {
		return m_momentum;
	}

	public void setMomentum(String m_momentum) {
		this.m_momentum = m_momentum;
	}

	public void setMomentum(double m_momentum) {
		this.m_momentum = Double.toString(m_momentum);
	}

	public String getNumEpochs() {
		return m_numEpochs;
	}

	public void setNumEpochs(String m_numEpochs) {
		this.m_numEpochs = m_numEpochs;
	}

	public void setNumEpochs(int m_numEpochs) {
		this.m_numEpochs = Integer.toString(m_numEpochs);
	}

	public String getPercentValidation() {
		return m_percentValidation;
	}

	public void setPercentValidation(String m_percentValidation) {
		this.m_percentValidation = m_percentValidation;
	}

	public void setPercentValidation(int m_percentValidation) {
		this.m_percentValidation = Integer.toString(m_percentValidation);
	}

	public String getSeed() {
		return m_seed;
	}

	public void setSeed(String m_seed) {
		this.m_seed = m_seed;
	}

	public void setSeed(int m_seed) {
		this.m_seed = Integer.toString(m_seed);
	}

	public String getTrainingDataFilename() {
		return m_trainingDataFilename;
	}

	public void setTrainingDataFilename(String m_trainingDataFilename) {
		this.m_trainingDataFilename = m_trainingDataFilename;
	}

	public String[] getOptions() {
		return m_options;
	}

	public void setOptions(String[] m_options) {
		this.m_options = m_options;
	}

	public Instances getTrainInstances() {
		return m_trainInstances;
	}

	public void setTrainInstances(Instances m_trainInstances) {
		this.m_trainInstances = m_trainInstances;
	}

	public MyMultilayerPerceptron getNetwork() {
		return m_network;
	}

	public void setNetwork(MyMultilayerPerceptron m_network) {
		this.m_network = m_network;
	}
	
}
