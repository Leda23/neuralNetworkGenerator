//package neuralNetworkModelInterpreter;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;

import org.isis.gme.bon.JBuilderModel;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.InterquartileRange;

/**
 * Class to execute the pre-processing tasks and compute the possible number of hidden neurons to address the problem
 *
 */
public class DataPreProcessor {
	/**
	 * Original data
	 */
	Instances data;
	/**
	 * File path to the original data
	 */
	Instances newData;
	/**
	 * Array to collect the possible number of hidden neurons
	 */
	ArrayList<Integer> hidden;
	/**
	 * Number of inputs
	 */
	int m_numInput;
	/**
	 * Number of outputs
	 */
	int m_numOutput;
	/**
	 * Number of data samples
	 */
	int m_numInputSample;
	/**
	 * Name of the output
	 */
	String m_outputName;
	/**
	 * COM component
	 */
	ModelInterpreter modelInterpeter;
	/**
	 * Metric to predict
	 */
	JBuilderModel metric;
	/**
	 * Properties contained in the process model
	 */
	ArrayList<String> processModelProperties;
	/**
	 * Current step of the data pre-processing
	 */
	int preprocessingStep;
	/**
	 * Total number of pre-processed steps
	 */
	final int preprocessingTotalSteps=4;
	
	/**
	 * Constructor
	 *
	 *@param trainingDataFilename path to the original data set
	 *@param modelInterpeter COM component
	 *@param metric the variable to predict
	 *@param processModelProperties the variable described in the process model
	 */
	public DataPreProcessor(String trainingDataFilename, ModelInterpreter modelInterpeter, JBuilderModel metric,ArrayList<String> processModelProperties){
		hidden = new ArrayList<Integer>();
		this.modelInterpeter=modelInterpeter;
		this.metric=metric;
		this.processModelProperties=processModelProperties;
		loadTrainingData(trainingDataFilename);
		preprocessingStep=0;
	}
	
	/**
	 * Constructor
	 *
	 *@param trainingDataFilename path to the original data set
	 *@param modelInterpeter COM component
	 */
	public DataPreProcessor(String trainingDataFilename, ModelInterpreter modelInterpeter){
		hidden = new ArrayList<Integer>();
		this.modelInterpeter=modelInterpeter;
		this.metric=null;
		this.processModelProperties=null;
		loadTrainingData(trainingDataFilename);
		preprocessingStep=0;
	}

	/**
	 * Function to load the data
	 */
	private void loadTrainingData(String trainingDataFilename){
		// Import data to a structured form (Instances) and select the column
		// that is to be predicted:
		// Instantiate a new Data Source with the location of the csv-file
		try {
			DataSource source = new DataSource(trainingDataFilename);
			data = source.getDataSet();
			m_outputName = data.attribute(data.numAttributes()-1).name();
			if (data.classIndex() == -1){
				data.setClassIndex(data.numAttributes() - 1);
			}
		} catch (Exception e) {
			modelInterpeter.setSubmit("Cancel");
			JOptionPane.showMessageDialog(null, "Impossible to load the data. Program aborted", "Error", JOptionPane.ERROR_MESSAGE );
		}
			
	}
	
	/**
	 * Function to pre-process the data: remove variables not contained in the process model, feature selection, and outlier selection.
	 */
	public void dataSetPreprocessing(){
		//Collect the properties of the model in an arrayList.
		try {
			//Verify that the metric in the model has the same name as the last column of the data set
			if(!(metric.getName().equals(this.m_outputName))){
				JOptionPane.showMessageDialog(null, "The metric you want to predict should be the last column of your data set. The name needs to be the same.", "Error", JOptionPane.ERROR_MESSAGE );
			}
			else{
				//Remove the variables that are not contained in the process model from the data
				String show = "";
				String [] dataSetParams = originalParams();
				String temp="";
				int variableRemoved=0;
				for(int i = dataSetParams.length-1; i >= 0; i--){
					if(!processModelProperties.contains(dataSetParams[i]))
					{
						variableRemoved++;
						removeAttributes(data, i);
						temp += "- "+dataSetParams[i] + "\n";
						
					}
				}
				if(!temp.equals("")){
					if(variableRemoved==1){
						show += "The following variable has been removed from the dataset using the information that you provided:"+"\n";
						show+=temp+"\n";
					}
					else{
						show += "The following variables have been removed from the dataset using the information that you provided:"+"\n";
						show+=temp+"\n";
					}
				}
				preprocessingStep++;
				//Outlier detection and elimination, instantiate newData
				outlierFiltering();
				preprocessingStep++;
				//Feature selection, modify newData
				newSelectAttributes();
				//Inform the user of the variable removed by feature selection
				ArrayList<String> removedFromAttSel = getAttributesRemoved();
				if(removedFromAttSel.size()!=0){
					if(removedFromAttSel.size()==1){
						show += "The following variable has been removed from the dataset using the feature selection:"+"\n";
					}
					else{
						show += "The following variables have been removed from the dataset using the feature selection:"+"\n";
					}
					for(int i = 0; i < removedFromAttSel.size(); i++)
					{
						show += "- "+removedFromAttSel.get(i)+"\n";
					}
				}
				preprocessingStep++;
				//Computation of the potential hidden neurons to address the problem
				populateHidden();
				modelInterpeter.setPotentialNumHiddenNeurons(hidden);
				modelInterpeter.setNumInputNeurons(m_numInput);
				modelInterpeter.setReducedDataSet(newData);
				modelInterpeter.setOutputType(m_outputName);	
				preprocessingStep++;
				if(!show.equals("")){
					JOptionPane.showMessageDialog(null, show, "Information", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		} catch (Exception e) {
			modelInterpeter.setSubmit("Cancel");
			preprocessingStep=preprocessingTotalSteps;
			JOptionPane.showMessageDialog(null, "Pre-processing of the data not completed. Program aborted", "Error", JOptionPane.ERROR_MESSAGE );
		}
	}
	
	/**
	 * Function to execute the outlier detection and elimation, instantiate newData
	 */
	private void outlierFiltering(){
		InterquartileRange filter= new InterquartileRange();
		try {
			filter.setInputFormat(data);
			newData = Filter
					.useFilter(data, filter);
			for(int k=newData.size()-1;k>=0;k--){
				if(newData.get(k).stringValue((newData.numAttributes()-1)).equals("yes")){
					data.remove(k);
				}
				else{
					if(newData.get(k).stringValue((newData.numAttributes()-2)).equals("yes")){
						data.remove(k);
					}
				}
			}
			newData=data;
		} catch (Exception e) {
			modelInterpeter.setSubmit("Cancel");
			JOptionPane.showMessageDialog(null, "Pre-processing of the data not completed. Program aborted", "Error", JOptionPane.ERROR_MESSAGE );
		}
	}

	/**
	 * Function to execute the feature selection, modify newData
	 */
	private void newSelectAttributes(){
		AttributeSelection filter = new AttributeSelection();
		CfsSubsetEval eval = new CfsSubsetEval();
		GreedyStepwise search = new GreedyStepwise();
		search.setSearchBackwards(false);	//Set the search as forwards
		filter.setEvaluator(eval);
		filter.setSearch(search);
		try {
			filter.SelectAttributes(newData);
			newData = filter.reduceDimensionality(newData);
			m_numInput = newData.numAttributes()-1; // number of input neurons
			m_numInputSample = newData.size()-1; // number of input sample
			m_numOutput = 1; // number of output neurons
		} catch (Exception e) {
			modelInterpeter.setSubmit("Cancel");
			JOptionPane.showMessageDialog(null, "Impossible to load the data. Program aborted", "Error", JOptionPane.ERROR_MESSAGE );
		} 
		
	}

	/**
	 * Function to compute the possible number of hidden neurons
	 */
	private void populateHidden(){
		// Using the multiple methods from the review paper, predict the number of hidden neurons needed in one layer
		li(m_numInput);
		tamura(m_numInput);
		zhang(m_numInput);
		jinchuan(m_numInput, m_numInputSample);
		shibata(m_numInput, m_numOutput);
		hunter(m_numInput);
		sheela(m_numInput);
		Collections.sort(hidden);
	}

	//Methods for finding the number of hidden neurons in one layer, named after the first author listed on each of the papers
	private void li (int n)
	{
		hidden.add((int)(Math.sqrt(1 + 8*n) -1)/2); // n is the number of input elements
	}
	private void tamura (int n)
	{
		hidden.add(n-1); // n is the number of input target relations; 
	}
	private void zhang (int n)
	{
		hidden.add(((int)(Math.pow(2,n))) / (n+1)); // Only tested for n greater than 3
	}
	private void jinchuan (int n, int np) 
	{
		int l = 1;	// # of layers 
		hidden.add((n+(int)(Math.sqrt(np))) / l); // Gives a high number for our sample data set but would be a better match for a small number of input samples (np)
	}
	private void shibata (int n, int no)
	{
		hidden.add((int)(Math.ceil(Math.sqrt(n*no)))); // n is the number of input elements, no is the number of output elements
	}
	private void hunter (int n)
	{
		hidden.add((int)(Math.ceil(Math.pow(2,n))) - 1);	// Used for a Fully Connected Cascade (FCC) Network
	}
	private void sheela (int n)
	{
		hidden.add((4*((int)Math.pow(n,2))+3)/(((int)Math.pow(n,2))-8)); // Gives an unreasonably high number of hidden neurons for small number of input neurons
	}

	/**
	 * function to collect the list of the removed attributes using the feature selection
	 * @return ArrayList<String> containing the attributes
	 */
	private ArrayList<String> getAttributesRemoved() {
		int temp;
		ArrayList<String> removedAttributes = new ArrayList<String>();
		for(int i = 0; i < data.numAttributes()-1; i++)
		{
			temp = 0;
			for(int j = 0; j < newData.numAttributes()-1; j++)
			{
				if((data.attribute(i).name()).equals(newData.attribute(j).name()))
					temp++;
			}
			if(temp !=1)
			{
				removedAttributes.add(data.attribute(i).name());
			}
		}
		return removedAttributes;
	}

	/**
	 * Function to collect the the list of variables in the original data set
	 * @return String[] containing the originial parameters
	 */
	private String[] originalParams(){
		String[] originalParameters = new String[data.numAttributes()-1];
		for(int i = 0; i < originalParameters.length; i++)
		{
			originalParameters[i] = data.attribute(i).name();
		}
		return originalParameters;
	}
	
	/**
	 * Function to remove an attribute at the position i from given data
	 * @param dataToProcess the given data
	 * @param index the index of the attribute in the data
	 */
	private void removeAttributes(Instances dataToProcess, int index){
		dataToProcess.deleteAttributeAt(index);
	}

	public int getPreprocessingStep() {
		return preprocessingStep;
	}

	public int getPreprocessingTotalSteps() {
		return preprocessingTotalSteps;
	}

	public void setMetric(JBuilderModel metric) {
		this.metric = metric;
	}

	public void setProcessModelProperties(ArrayList<String> processModelProperties) {
		this.processModelProperties = processModelProperties;
	}
}