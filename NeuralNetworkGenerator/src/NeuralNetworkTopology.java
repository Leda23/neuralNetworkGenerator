//package neuralNetworkModelInterpreter;

import java.util.HashMap;
import java.util.Map;

public class NeuralNetworkTopology {

	Map<String, Double> m_biasNameToValue = new HashMap<>();
	Map<String, Double> m_weightNameToValue = new HashMap<>();

	NeuralNetworkTopology(Map<String, Double> biasNameToValue,
			Map<String, Double> weightNameToValue) {
		m_biasNameToValue = biasNameToValue;
		m_weightNameToValue = weightNameToValue;
	}

	public Map<String, Double> getBiasNameToValue() {
		return m_biasNameToValue;
	}

	public void setBiasNameToValue(Map<String, Double> biasNameToValue) {
		m_biasNameToValue = biasNameToValue;
	}

	public Map<String, Double> getWeightNameToValue() {
		return m_weightNameToValue;
	}

	public void setWeightNameToValue(Map<String, Double> weightNameToValue) {
		m_weightNameToValue = weightNameToValue;
	}

}
