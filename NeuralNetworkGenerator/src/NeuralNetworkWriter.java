//package neuralNetworkModelInterpreter;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.dmg.pmml.Header;
import org.dmg.pmml.PMML;

import weka.core.Instances;

public class NeuralNetworkWriter {
	
	MyMultilayerPerceptron bestMLP;
	Instances dataSet;

	public NeuralNetworkWriter(MyMultilayerPerceptron bestMLP, Instances originalDataSet) {
		this.bestMLP=bestMLP;
		this.dataSet=originalDataSet;
	}

	
	/**
	 * A Predictive Model Markup Language (PMML) document is created that
	 * represents this network as a PMML NeuralNetwork model:
	 * 
	 * @return A PMML document representing the neural network.
	 */
	public String networkToPMML(Instances trainingInstances) throws JAXBException {
		// Use jpmml to convert mlp to PMML:
		StringWriter writer = new StringWriter();
		Header header = new Header().setCopyright("NIST").setDescription(
				"small example");

		// Get the PMML-related information from the neural network:
		PMML pmml = bestMLP.toPMML("4.1", header, dataSet,trainingInstances);

		JAXBContext jc = JAXBContext.newInstance(pmml.getClass());
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.marshal(pmml, writer);

		return writer.toString();
	}

	/**
	 * The internal structure of this network is converted into a Portable
	 * Format for Analytics (PFA) document that will produce identical outputs
	 * as this network, given identical inputs (barring minor numerical
	 * differences of implementations).
	 * 
	 * @return A PFA document representing this network.
	 */
	public String networkToPFA() {
		String open = "[";
		String close = "]";
		String delim = ",";
		String pyWeights = "";
		List<List<List<Double>>> weights = bestMLP.getAllWeights();

		// Construct a string holding the network's weights and biases inside
		// a nested Python list:
		pyWeights += open;
		for (int i = 0; i < weights.size(); ++i) {
			List<List<Double>> layerWeights = weights.get(i);
			pyWeights += open;

			for (int j = 0; j < layerWeights.size(); ++j) {
				List<Double> nodeWeights = layerWeights.get(j);
				pyWeights += open;

				for (int k = 0; k < nodeWeights.size(); ++k) {
					pyWeights += nodeWeights.get(k).toString();
					if (k < nodeWeights.size() - 1) {
						pyWeights += delim;
					}
				}
				pyWeights += close;

				if (j < layerWeights.size() - 1) {
					pyWeights += delim;
				}
			}
			pyWeights += close;

			if (i < weights.size() - 1) {
				pyWeights += delim;
			}
		}
		pyWeights += close;
		return pyWeights;
	}
	
	/**
	 * Insert a string into an array of strings, shifting elements to the right.
	 * 
	 * @param strs
	 *            The original array of strings.
	 * @param pos
	 *            The index within the array to place the new string.
	 * @param str
	 *            The string that will be added
	 * @return A new array that contains the original array's strings along with
	 *         the new string.
	 */
	public static String[] addPos(String[] strs, int pos, String str) {
		String[] result = new String[strs.length + 1];

		for (int i = 0; i < pos; ++i) {
			result[i] = strs[i];
		}
		result[pos] = str;
		for (int i = pos + 1; i < result.length; ++i) {
			result[i] = strs[i - 1];
		}
		return result;
	}
}
