//package neuralNetworkModelInterpreter;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class MyProgressBarForDataPreprocessing extends Thread{
	JFrame progressFrame;
	JProgressBar progressBar;
	JLabel text;
	boolean run;
	static final int MINIMUM=0;
	static final int MAXIMUM=100;
	ModelInterpreter modelInterpreter;
	DataPreProcessor datapreprocessor;

	public MyProgressBarForDataPreprocessing(ModelInterpreter modelInterpreter1,DataPreProcessor datapreprocessor1) {
		this.modelInterpreter=modelInterpreter1;
		this.datapreprocessor=datapreprocessor1;
		// Cr�er un objet de la Barre de progression
		progressBar = new JProgressBar( );
		 
		// D�finir la valeur initiale de la barre de progression
		progressBar.setMinimum(MINIMUM);
		// D�finir la valeur maximale de la barre de progression
		progressBar.setMaximum(MAXIMUM);
		progressBar.setStringPainted(true);
		// Cr�er un JPanel et ajouter la barre de progression dans le JPanel
		progressFrame=new JFrame("Processing");
		Dimension dim=new Dimension(300, 100);
		progressFrame.setPreferredSize(dim);
		progressFrame.setLocationByPlatform(true);
		JPanel pnl=new JPanel();
		JLabel textStatic=new JLabel();
		textStatic.setText("Data pre-processing");
		text=new JLabel();
		text.setText("");
		pnl.add(textStatic);
		pnl.add(text);
		pnl.add(progressBar);
		JButton cancelButton= new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modelInterpreter.setSubmit("Cancel");
				progressFrame.dispose();
				run=false;
			}
		});
		pnl.add(cancelButton);
		progressFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		progressFrame.setContentPane(pnl);
		progressFrame.pack();
	}


	@Override
	public void run() {
		progressFrame.setVisible(true);
		run=true;
		long millis1 = System.currentTimeMillis();
		while (run==true&&progressBar.getValue()<100) {
			int progress;
			Double temp;
			if(this.datapreprocessor.getPreprocessingStep()==this.datapreprocessor.getPreprocessingTotalSteps()){
				temp=100.0;
			}
			else{
				temp=(double)this.datapreprocessor.getPreprocessingStep()/((double)this.datapreprocessor.getPreprocessingTotalSteps())*100;
			}
			progress=temp.intValue();
			updateBar(progress);
			long millis2=System.currentTimeMillis();
			long millis=millis2-millis1;
	        long hours = TimeUnit.MILLISECONDS.toHours(millis);
	        millis -= TimeUnit.HOURS.toMillis(hours);
	        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
	        millis -= TimeUnit.MINUTES.toMillis(minutes);
	        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

	        StringBuilder sb = new StringBuilder(64);
	        if(hours>0){
		        sb.append(hours);
		        sb.append("h");
	        }
	        if(minutes>0||minutes==0&&hours>0){
		        sb.append(minutes);
		        sb.append("m");
	        }
	        sb.append(seconds);
	        sb.append("s");
			text.setText(sb.toString());
		}
		progressFrame.dispose();
	}

	public void updateBar(int newValue){
		progressBar.setValue(newValue);
	}

	public void setDataPreprocessor(DataPreProcessor datapreprocessor){
		this.datapreprocessor=datapreprocessor;
	}
}
