//package neuralNetworkModelInterpreter;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import weka.core.Instances;

//import org.isis.gme.bon.JBuilderFolder;
//import org.isis.gme.bon.JBuilderModel;

public class FrameforSavingCSVFile extends Thread {

	static Dimension dimFrame = new Dimension(600, 200);
	JTextField filePathField;
	ModelInterpreter modelInterpreter;
	String type;
	File saveFileName;
	JFrame frame;

	public FrameforSavingCSVFile(ModelInterpreter modelInterpreter) {
		super();
		this.modelInterpreter = modelInterpreter;
	}

	@Override
	public void run() {
		frameConfiguration();
		while (frame.isVisible()) {

		}
	}

	public void frameConfiguration() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setSize(dimFrame);
		frame.setPreferredSize(dimFrame);
		Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((screen.width - frame.getWidth()) / 2,
				(screen.height - frame.getHeight()) / 2);
		configureNorthPanel(frame);
		configureCenterPanel(frame, type);
		configureSouthPanel(frame);
		frame.repaint();
		frame.validate();
		frame.setVisible(true);
	}
	private void configureNorthPanel(JFrame frame) {
		JPanel panel = new JPanel();
		String text = "The Neural Network has been built.";
		JLabel label = new JLabel(text);
		panel.add(label);
		frame.add(panel, BorderLayout.NORTH);
		frame.repaint();
		frame.validate();
	}
	private void configureCenterPanel(final JFrame frame, String type) {
		double width = dimFrame.width * 0.8;
		double height = dimFrame.height * 0.5;
		final JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
		JPanel mainPanel = new JPanel();
		JPanel fileChooserPanel = new JPanel();
		fileChooserPanel.setPreferredSize(new Dimension((int) width,
				(int) height));
		String text = "Please provide your save location:";
		JLabel label = new JLabel(text);
		fileChooserPanel.add(label);
		filePathField = new JTextField();
		width = dimFrame.width * 0.6;
		height = 24;
		filePathField.setPreferredSize(new Dimension((int) width, (int) height));
		fileChooserPanel.add(filePathField);
		JButton browseButton = new JButton("Browse");
		browseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fileChooser.showOpenDialog(frame);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					saveFileName = fileChooser.getSelectedFile();
					filePathField.setText(saveFileName.getAbsolutePath());
				}

			}
		});
		fileChooserPanel.add(browseButton);
		mainPanel.add(fileChooserPanel);
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.repaint();
		frame.validate();
	}
	private void configureSouthPanel(final JFrame frame) {
		JPanel panel = new JPanel();
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String newData = "";
				Instances reducedData = modelInterpreter.getReducedDataSet();
				for(int i = 0; i < reducedData.numAttributes(); i++){
					if(i>0){newData += ",";}
					newData += reducedData.attribute(i).name();
				}
				newData+="\n";
				for(int j = 0; j < reducedData.numInstances(); j++){
						newData += reducedData.instance(j);
					newData+="\n";
				}
				
				try {
					FileWriter fw = new FileWriter(saveFileName.getAbsolutePath() + "\\newData.csv");
					fw.write(newData);
					fw.close();
					fw = new FileWriter(saveFileName.getAbsolutePath() + "\\newData.arff");
					fw.write(reducedData.toString());
					fw.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				try {
					modelInterpreter.setFilePathField(saveFileName.getAbsolutePath() + "\\newData.csv");
					modelInterpreter.setSubmit("Submit");
					frame.dispose();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modelInterpreter.setSubmit("Cancel");
				frame.dispose();
			}
		});
		panel.add(saveButton);
		panel.add(cancelButton);
		frame.add(panel, BorderLayout.SOUTH);
		frame.repaint();
		frame.validate();
	}

	public JTextField getFilePathField() {
		return filePathField;
	}
}