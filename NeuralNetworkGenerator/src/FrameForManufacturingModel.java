//package neuralNetworkModelInterpreter;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import org.isis.gme.bon.JBuilderModel;

/**
* Configure a Frame that is shown when a user executes the interpreter from a process model.
*/
public class FrameForManufacturingModel extends Thread {
	/**
	 * Dimension of the frame <width, height>
	 */
	static Dimension dimFrame = new Dimension(600, 150);
	/**
	 * Text field that will contain the path to the file containing the data
	 */
	JTextField dataFilePathField;
	/**
	 * The COM component executed by GME
	 */
	ModelInterpreter modelInterpreter;
	/**
	 * The frame to be created
	 */
	JFrame frame;
	/**
	 * The process model
	 */
	JBuilderModel focus;

	/**
	* Constructor
	* 
	* @param modelInterpreter the COM component executed by GME
	* @param focus the process model used by the interpreter
	*/
	public FrameForManufacturingModel(ModelInterpreter modelInterpreter, JBuilderModel focus) {
		super();
		this.modelInterpreter = modelInterpreter;
		this.focus=focus;
	}

	@Override
	public void run() {
		frameConfiguration();
		//The thread runs as long as the frame is visible
		while (frame.isVisible()) {

		}
	}

	/**
	* Configuration of the frame
	*/
	public void frameConfiguration() {
		frame = new JFrame("Data acquisition");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setSize(dimFrame);
		frame.setPreferredSize(dimFrame);
		//center the frame in the screen
		Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((screen.width - frame.getWidth()) / 2,
				(screen.height - frame.getHeight()) / 2);
		configureCenterPanel();
		configureSouthPanel();
		frame.setVisible(true);
	}

	/**
	 * Configure the field for the data path and the browse button to find the csv file.
	 */
	private void configureCenterPanel() {
		double width = dimFrame.width * 0.8;
		double height = dimFrame.height * 0.7;
		//Main panel
		JPanel centerPanel = new JPanel();
		centerPanel.setPreferredSize(new Dimension((int) width,(int) height));
		//Contain the text field and the browse button
		JPanel fileChooserPanel = new JPanel();
		fileChooserPanel.setPreferredSize(new Dimension((int) width,(int) height));
		//Include the label
		JLabel labelForData = new JLabel("Please provide your data file:");
		fileChooserPanel.add(labelForData);
		//Include the text field
		dataFilePathField = new JTextField();
		dataFilePathField.setEditable(false);
		width = dimFrame.width * 0.6;
		height = 24;
		dataFilePathField.setPreferredSize(new Dimension((int) width,(int) height));
		fileChooserPanel.add(dataFilePathField);
		//Include the text button
		JButton browseButtonForData = new JButton("Browse");
		browseButtonForData.addActionListener(new ActionListener() {
			//Display a file chooser for CSV files on click
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				FileFilter filter = new FileFilter() {
					public String getDescription() {
						return "CSV files";
					}
					
					public boolean accept(File f) {
						return f.isDirectory()||f.getName().endsWith(".csv");
					}
				};
				fileChooser.setFileFilter(filter);
				int returnVal = fileChooser.showOpenDialog(frame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					if(file.exists()){
						dataFilePathField.setText(file.getAbsolutePath());
					}
					else{
						dataFilePathField.setText("");
					}
				}
			}
		});
		fileChooserPanel.add(browseButtonForData);
		centerPanel.add(fileChooserPanel);
		frame.add(centerPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Configure the submit and cancel button of the frame.
	 */
	private void configureSouthPanel() {
		JPanel southPanel = new JPanel();
		JButton submitButton = new JButton("Submit");
		submitButton.addActionListener(new ActionListener() {
			//forward the path of the file to the COM component on click
			public void actionPerformed(ActionEvent arg0) {
				if(dataFilePathField.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Please provide the file path", "Error", JOptionPane.ERROR_MESSAGE  );
				}
				else{
					String dataFileName = dataFilePathField.getText();
					modelInterpreter.setLocationOriginalDataFile(dataFileName);
					modelInterpreter.setSubmit("Submit");
					frame.dispose();
				}
			}
		});
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modelInterpreter.setSubmit("Cancel");
				frame.dispose();
			}
		});
		southPanel.add(submitButton);
		southPanel.add(cancelButton);
		frame.add(southPanel, BorderLayout.SOUTH);
	}	
}
