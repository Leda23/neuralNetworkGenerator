Install GME:
============
Please install GME from here:
https://forge.isis.vanderbilt.edu/gme/
Choose the 32 bit version (GME-17.3.13.msi)

Install GME Meta-model:
=======================
The GME Meta-model for process specification is in the file M3.xme. 
1. Open GME, and select “open”, and open the file M3.xme. 
2. Click on the icon in the toolbar to start the MetaGME Interpreter (icon looks like a black gear). 
3. Click OK to the pop up questions, and register the interpreter. 

Load the instance model:
========================
The instance model is in the file MillingAnalytics.xme.
1. Open a new GME window. Select “open”, and open the file “MillingAnalytics.xme”.
2. Click OK to the pop up questions.
You should now be able to see the instance model.

Install the Neural Network Generator:
=====================================
The source code for the Neural Network Generator is in the "NeuralNetworkGenerator" folder. We have also provided pre-compiled binaries in the "bin" folder. Follow these instructions to register the Neural Network Generator in GME.
1. Open your file explorer and go to the \SDK\Java folder within your GME installation (usually in Program files x86). 
2. Run the file “JavaCompRegister.exe” (follow prompts to allow user access if requested)
3. A pop up box will open. Enter the following information:
    i. Name: Enter "NeuralNetworkGenerator" 
   ii. Description: Enter "Neural Network Generator"
  iii. Classpath: Enter the entire path to the neuralNetworkGenerator.jar file in the bin folder, and the entire path to the files "pmml-model-1.2.2.jar" and "weka.jar" in the "lib" folder, with the paths separated by semi-colons. For example, "c:/.../bin/neuralNetworkGenerator.jar;c:/.../lib/pmml-model-1.2.2.jar;c:/.../lib/weka.jar"
   iv. Class: Enter "ModelInterpreter"
    v. Paradigms: Leave as is (*)
   vi. Menu/Tooltip: Enter "Neural Network Generator"
4. Click “Register”.

Run the Neural Network Generator Example:
=========================================
1. Open the instance model in GME (MillingAnalytics.xme) as described above
2. In the tree view on the right in GME, open "RootFolder" and the contained items, until you locate "MillingAnalytics". 
3. Double click on "MillingAnalytics". This will open the example model in the main window.
4. For the first use, the generator needs to be registered. Click on the "Tools" menu, click on "Register Components", find the Neural Network Generator and select enable.]
5. Click on the icon Mga.Interpreter.NeuralNetworkGenerator in the tool bar (or click on the "Tools" menu, click on "Mga.Interpreter.NeuralNetworkGenerator", and run the neuralNetworkGenerator interpreter.
6. A new window pops up. Select the data provided in the frolde named "NN scenario" and submit.
7. An information windows presents the result of the feature selection, click OK.
8. A window to set up the neural network training pops up. Optional: Change the default parameter values. 
9. Click "Export in PMML".
The training starts and a window shows progress.
At the end, another windows pops up to save the pmml file that has been generated.
Choose the location to save the pmml file and name the file.